Web Worklog
--------------

**Link to company repo: http://gitlab.c-call.eu/keios-apps/webworklog**

####Under development / early alpha version

This is a simple app, written as an earily experiment with Python by one of junior devs.

It provides the panel to count work and calculate rates for freelance tasks. It uses Python2.7 with Flask framework.

You can find a screenshot here:
http://well-designed.eu/themes/portfolio/assets/img/portfolio/webworklog.png


###Work Status

####DONE:
- Customer signup
- Customer create
- Customer login page

- Customer select
- Worktime History
- Worktime Information
- Worktime Counter


####TODO:
- table generator for customer
- site lock if not authorized
- client select + client create connection
- force addition, change password, remove client, change rate
- statistics
- refactoring